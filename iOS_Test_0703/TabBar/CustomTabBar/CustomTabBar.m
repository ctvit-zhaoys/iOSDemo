//
//  CustomTabBar.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//  普通 TabBar

#import "CustomTabBar.h"

@implementation CustomTabBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self addSubview:self.tabBarImgView];
        [self addSubview:self.centerButton];
        
        // 去除顶部横线
        [self setBackgroundImage:[UIImage new]];
        [self setShadowImage:[UIImage new]];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    // 重设 TabBar 位置
    self.frame = CGRectMake(0, SCREEN_HEIGHT - 67 - 10, SCREEN_WIDTH, 67);
    
    // 设置中间按钮
    self.centerButton.frame = CGRectMake(0, -15, 55, 55);
    self.centerButton.center = CGPointMake(SCREEN_WIDTH / 2, 15);
    
    // 设置其他TabBarItem frame
    CGFloat tabBarButtonW = (SCREEN_WIDTH - 20) / 5;
    CGFloat TabBarButtonIndex = 0;
    for (UIView *child in self.subviews) {
        Class class = NSClassFromString(@"UITabBarButton");
        if([child isKindOfClass:class]) {
            CGRect frame = CGRectMake(TabBarButtonIndex * tabBarButtonW + 10, 3, tabBarButtonW, 49);
            child.frame = frame;
            TabBarButtonIndex ++;
        }
    }
}

- (void)centerTabBarBtnEvent {
    
}

- (UIButton *)centerButton {
    if(!_centerButton) {
        _centerButton = [[UIButton alloc]init];
        _centerButton.clipsToBounds = YES;
        _centerButton.layer.cornerRadius = 45 / 2;
        [_centerButton addTarget:self action:@selector(centerTabBarBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    return _centerButton;
}

- (UIImageView *)tabBarImgView {
    if(!_tabBarImgView) {
        _tabBarImgView = [[UIImageView alloc]init];
        _tabBarImgView.image = [UIImage imageNamed:@"icon_home"];
    }
    return _tabBarImgView;
}

@end
