//
//  CustomTabBarViewController.h
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomTabBarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
