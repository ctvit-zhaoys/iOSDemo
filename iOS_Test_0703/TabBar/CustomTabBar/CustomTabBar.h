//
//  CustomTabBar.h
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@interface CustomTabBar : UITabBar

@property (nonatomic, strong) UIImageView *tabBarImgView;

@property (nonatomic, strong) UIButton *centerButton;

@end

NS_ASSUME_NONNULL_END
