//
//  CommonTabBarViewController.h
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonTabBarViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
