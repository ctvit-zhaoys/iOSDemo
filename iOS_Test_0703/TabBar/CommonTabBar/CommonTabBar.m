//
//  CommonTabBar.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import "CommonTabBar.h"

@implementation CommonTabBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self addSubview:self.tabBarImgView];
        
        // 去除顶部横线
        [self setBackgroundImage:[UIImage new]];
        [self setShadowImage:[UIImage new]];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    // 重设 TabBar 位置
    self.frame = CGRectMake(0, SCREEN_HEIGHT - 49 - 10, SCREEN_WIDTH, 49);
    
    // 设置 TabBarItem frame
    CGFloat tabBarButtonW = (SCREEN_WIDTH - 20) / 2;
    CGFloat TabBarButtonIndex = 0;
    for (UIView *child in self.subviews) {
        Class class = NSClassFromString(@"UITabBarButton");
        if([child isKindOfClass:class]) {
            CGRect frame = CGRectMake(TabBarButtonIndex * tabBarButtonW + 10, 3, tabBarButtonW, 49);
            child.frame = frame;
            TabBarButtonIndex ++;
        }
    }
}

- (UIImageView *)tabBarImgView {
    if(!_tabBarImgView) {
        _tabBarImgView = [[UIImageView alloc]init];
    }
    return _tabBarImgView;
}

@end
