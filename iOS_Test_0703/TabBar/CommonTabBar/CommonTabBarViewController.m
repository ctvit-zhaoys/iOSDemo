//
//  CommonTabBarViewController.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import "CommonTabBarViewController.h"
#import "CommonTabBar.h"

#import "YSFirstViewController.h"
#import "YSSecondViewController.h"

@interface CommonTabBarViewController ()

@end

@implementation CommonTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CommonTabBar *tabBar = [[CommonTabBar alloc]init];
    [self setValue:tabBar forKey:@"tabBar"];
    
    [self setupOneChildViewController:[[YSFirstViewController alloc]init] title:@"首页" image:@"tabbar_oly_hot" selectedImage:@"tabbar_oly_hot_selected" tag:0];
    
    [self setupOneChildViewController:[[YSSecondViewController alloc]init] title:@"首页" image:@"tabbar_oly_match" selectedImage:@"tabbar_oly_match_selected" tag:1];

}

/**
 *  初始化一个子控制器
 *
 *  @param vc            子控制器
 *  @param title         标题
 *  @param image         图标
 *  @param selectedImage 选中的图标
 */
- (void)setupOneChildViewController:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage tag:(NSInteger)tag {
    vc.tabBarItem.title = title;
    // 设置选中 Title 颜色
    [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blueColor],NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    vc.tabBarItem.tag = tag;

    if (image.length) { // 图片名有具体值
        vc.tabBarItem.image = [[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
    }
    
    [self addChildViewController:vc];
}


@end
