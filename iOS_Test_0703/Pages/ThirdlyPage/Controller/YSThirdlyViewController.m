//
//  YSThirdlyViewController.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import "YSThirdlyViewController.h"

@interface YSThirdlyViewController ()

@end

@implementation YSThirdlyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
