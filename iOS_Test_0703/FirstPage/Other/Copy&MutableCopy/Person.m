//
//  Person.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/4.
//

#import "Person.h"

@implementation Person

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    Person *person = [Person allocWithZone:zone];
    person.name = self.name;
    person.maryDatas = [self.maryDatas mutableCopy];
    
    return person;
}

- (nonnull id)mutableCopyWithZone:(nullable NSZone *)zone {
    Person *person = [Person allocWithZone:zone];
    person.name = self.name;
    person.maryDatas = [self.maryDatas mutableCopy];
    
    return person;
}

@end
