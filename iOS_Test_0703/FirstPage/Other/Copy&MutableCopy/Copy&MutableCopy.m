//
//  Copy&MutableCopy.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/4.
//

/**
 * 字符串：NSString NSMutableString
 * 数组：NSArray NSMutableArray
 * 字典：NSDictionary NSMutableDictionary
 * 数据：NSData NSMutableData
 * 集合：NSSet NSMutableSet
 *
 * 深拷贝：拷贝源对象生成副本对象，两者在不同的内存。对不可变类型调用mutableCopy、可变类型调用copy和mutableCopy都是深拷贝。
 *
 * 浅拷贝：不拷贝源对象，不会生成副本对象，只拷贝指向源对象的指针。对不可变对象调用copy方法的情况就是浅拷贝。
 *
 * mutableCopy返回的是可变对象。
 */

#import "Copy&MutableCopy.h"
#import "Person.h"

@implementation Copy_MutableCopy

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        [self createCopyFuc];
        
        [self createMutableCopyFuc];
        
        [self createPersonFuc];
        
    }
    return self;
}


/**
 * 其他类是不区分是否可变的，如：自定义类、UIKit框架下的类。
 * 拷贝类型只有深拷贝，没有浅拷贝，使用copy和mutableCopy没有区别，返回的都是可变对象。
 * 使用时，需要先遵循NSCoping或NSMutableCopying协议。
 *
 */
- (void)createPersonFuc {
    Person *person = [[Person alloc]init];
    person.name = @"张三";
    person.maryDatas = [NSMutableArray arrayWithObject:@"1"];
    
    Person *person2 = [person copy];
    person2.name = @"王五";
}


/**
 * mutableCopy
 *
 */
- (void)createMutableCopyFuc {
    NSDictionary *dic1 = @{@"key1" : @"value1"};
    NSDictionary *dic2 = [dic1 mutableCopy];
    
    /// 两者地址不同，内容相同，为深拷贝
    NSLog(@"%p ------ %p", dic1, dic2); // 0x1029044c8 ------ 0x28254cc40
    
    [dic2 setValue:@"value2" forKey:@"key2"];
    
    
    NSMutableDictionary *dic3 = [NSMutableDictionary dictionaryWithObject:@"value1" forKey:@"key1"];
    NSDictionary *dic4 = [dic3 mutableCopy];
    
    /// 两者地址不同，内容相同，为深拷贝
    NSLog(@"%p ------ %p", dic3, dic4); // 0x2828b31e0 ------ 0x2828b3200
    
    [dic4 setValue:@"value2" forKey:@"key2"];
}


/**
 * copy
 *
 */
- (void)createCopyFuc {
    NSDictionary *dic1 = @{@"key1" : @"value1"};
    NSDictionary *dic2 = [dic1 copy];
    
    /// 两者地址相同，指向同一个对象，为浅拷贝
    NSLog(@"%p ------ %p", dic1, dic2); // 0x1020304b8 ------ 0x1020304b8
    
//    [dic2 setValue:@"value2" forKey:@"key2"]; // 报错

    
    NSMutableDictionary *dic3 = [NSMutableDictionary dictionaryWithObject:@"value1" forKey:@"key1"];
    NSDictionary *dic4 = [dic3 copy];
    
    /// 两者地址不同，内容相同，为深拷贝，返回的是不可变对象
    NSLog(@"%p ------ %p", dic3, dic4); // 0x283f46660 ------ 0x283f465c0
    
//    [dic4 setValue:@"value2" forKey:@"key2"]; // 报错

}


@end
