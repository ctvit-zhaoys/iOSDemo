//
//  Person.h
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/4.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject<NSCopying, NSMutableCopying>

@property (nonatomic,   copy) NSString *name;

@property (nonatomic, strong) NSMutableArray *maryDatas;

@end

NS_ASSUME_NONNULL_END
