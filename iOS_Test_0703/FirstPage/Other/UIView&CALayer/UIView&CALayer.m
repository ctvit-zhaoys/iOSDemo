//
//  UIView&CALayer.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/4.
//
/**
 《UIView & CALayer》
 
 1、UIView 主要是对显示内容的管理 | CALayer主要是对显示的绘制
 
 2、UIView属于UIKit框架 | CALayer属于QuartzCore框架下的，
 UIKit使用UIResponder作为响应对象，来响应系统传递来的事件并进行处理，所以UIView是可以响应事件的，CALayer直接继承于NSObject不能响应事件
 
 3、一个Layer的frame是由anchorPoint，position，bounds和transform共同决定的 | 而一个UIView的frame只是简单的返回了CALayer的frame，同样bounds和center也是返回了CALayer的属性
 
 4、UIView依赖于CALayer提供的内容，CALayer依赖UIView提供的容器来显示绘制的内容，归根到底CALayer是一切的基础，UIView是一个特殊的CALayer的实现，并添加了响应事件的能力。
 
 5、隐式动画：当改变一个layer的任何一个属性时，都会触发一个从旧值到新值的简单动画，这就是所谓的隐式动画，
 但是，当layer附加到view中时，UIView默认情况下禁止了CALayer的动画，但是在animation block中又重启了他们
 
 
 任何可动画的Layer属性改变时，Layer都会寻找并运行合适的'action'来实现这个改变。在Core Animation的专业术语中就把这样的动画统称为动作'action'

 
 @property(nullable, weak) id <CALayerDelegate> delegate;
 - (nullable id<CAAction>)actionForLayer:(CALayer *)layer forKey:(NSString *)event;

 Layer通过向它的delegate发送 actionForLayer:forKey: 消息来询问提供一个对应属性变化的action，delegate通过返回以下三者之一来进行响应：
 1.返回一个动作对象，这种情况下Layer将使用这个动作
 2.返回一个nil,这样Layer就会到其他地方继续寻找
 3.返回一个NSNull对象，告诉Layer这里不需要执行一个动作，搜索也会到此为止
 当Layer在背后支持一个View时，View就是它的delegate
 */

#import "UIView&CALayer.h"

@implementation UIView_CALayer

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
