//
//  Dingshiqi.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import "Dingshiqi.h"

@interface Dingshiqi ()

@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) CADisplayLink *displayTimer;

@property (nonatomic, strong) dispatch_source_t sourceTimer;

@end

@implementation Dingshiqi

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        [self createTimer];
        
        [self createCADisplayLink];
        
        [self createDispatch_source_t];
        
        [self createGCDAfter];
        
        
    }
    return self;
}


#pragma mark - performSelector

- (void)createperformSelector {
    [self performSelector:@selector(performSelectorAfterFuc) withObject:nil afterDelay:10.0];
}

- (void)performSelectorAfterFuc {
    NSLog(@"");
}


#pragma mark - dispatch_after

/**
 * 使用 dispatch_after 可以将一个需要延迟执行的任务放入一个队列中进行延迟执行。
 *
 *
 */
- (void)createGCDAfter {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSLog(@"");
    });
}


#pragma mark - dispatch_source_t

/**
 * 时间准确
 *
 * 可以使用子线程，解决定时间跑在主线程上卡UI问题
 *
 * 需要将dispatch_source_t timer设置为成员变量，不然会立即释放
 *
 */
- (void)createDispatch_source_t {
    // 创建全局队列
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    // 使用全局队列创建定时器
    self.sourceTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    // 定时器延迟时间
    NSTimeInterval delayTime = 1.0f;
    
    // 定时器间隔时间
    NSTimeInterval timeInterval = 1.0f;
    
    // 设置开始时间
    dispatch_time_t startDelayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC));
    
    // 设置定时器
    dispatch_source_set_timer(self.sourceTimer, startDelayTime, timeInterval * NSEC_PER_SEC, 0.1 * NSEC_PER_SEC);
    
    // 执行事件
    dispatch_source_set_event_handler(self.sourceTimer, ^{
        NSLog(@"%s", __func__);
        
        // 销毁定时器
//        dispatch_source_cancel(self.sourceTimer);
    });
    
    // 启动定时器
    dispatch_resume(_sourceTimer);
    
}


#pragma mark - CADisplayLink

/**
 * 当把CADisplayLink对象add到runloop中后，selector就能被周期性调用，类似于重复的NSTimer被启动了；
 * 执行invalidate操作时，CADisplayLink对象就会从runloop中移除，selector调用也随即停止，类似于NSTimer的invalidate方法。
 *
 * 屏幕刷新时调用CADisplayLink是一个能让我们以和屏幕刷新率同步的频率将特定的内容画到屏幕上的定时器类。
 *
 * 延迟iOS设备的屏幕刷新频率是固定的，CADisplayLink在正常情况下会在每次刷新结束都被调用，精确度相当高。
 *
 */
- (void)createCADisplayLink {
    self.displayTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(handleDisplayLink:)];
    [self.displayTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)handleDisplayLink:(CADisplayLink *)displaylinkTimer {
    NSLog(@"%s ------ %ld", __func__, displaylinkTimer.preferredFramesPerSecond);
    
    // 销毁定时器
    [self.displayTimer invalidate];
    self.displayTimer = nil;
}


#pragma mark - NSTimer

/**
 * NSTimer
 *
 * 存在延迟 不管是一次性的还是周期性的timer的实际触发事件的时间，都会与所加入的RunLoop和RunLoop Mode有关，如果此RunLoop正在执行一个连续性的运算，timer就会被延时出发。
 *
 * 必须加入Runloop
 *
 */
- (void)createTimer {
    // 一、初始化
//    self.timer = [NSTimer scheduledTimerWithTimeInterval:3.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        // 执行操作
//        NSLog(@"执行操作");
//    }];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(timerStart:) userInfo:nil repeats:YES];
    
    // 二、加入到 Runloop循环池
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    
    // 开启定时器
//    [self.timer fire];
}

- (void)timerStart:(NSTimer *)timer {
    NSLog(@"%s ------ %lf", __func__, timer.timeInterval);
    
    // 三、销毁定时器
    [self.timer invalidate];
    self.timer = nil;
}

@end
