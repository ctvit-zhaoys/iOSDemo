//
//  iOSBlock.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/6.
//

#import "iOSBlock.h"

@implementation iOSBlock

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {

        [self createAutoBlockFuc];
        
        [self createStaticBlockFuc];
        
        [self createBlockFuc];
        
        [self createSetBlockFuc];
        
        [self createBlock];
        
    }
    return self;
}

- (void)createBlock {
    void (^block)(void) = ^{
        NSLog(@"%@",self); // <YSFirstViewController: 0x10130ca40>
    };
    block();
}

- (void)createSetBlockFuc {
    // 是因为block截获auto变量时是值传递，不能访问到auto变量的指针地址，所以无法修改
//    int a = 10;
//    int(^block1)(int) = ^int(int num) {
//        a = 2; // Variable is not assignable (missing __block type specifier)
//        return a;
//    };
    
    /// 一
    __block int a = 10;
    int(^block2)(int) = ^int(int num) {
        a = 2;
        return a;
    };
    
    /// 二
    static int b = 10;
    int(^block3)(int) = ^int(int num) {
        b = 2;
        return b;
    };
}


#pragma mark - __block

/**
 __block 修饰符
 */
- (void)createBlockFuc {
    __block int a = 10;
    int(^block)(int) = ^int(int num) {
        return a * num;
    };
    a = 2;
    NSLog(@"int block 的结果是 = %d", block(10)); // 20
}


#pragma mark - static

/**
 局部变量
 static静态变量
 */
- (void)createStaticBlockFuc {
    static int a = 10;
    int(^block)(int) = ^int(int num) {
        return a * num;
    };
    a = 2;
    NSLog(@"static block 的结果是 = %d", block(10)); // 20
}


#pragma mark - auto

/**
 局部变量
 auto变量 离开作用域(大括号),会自动释放的变量
 */
- (void)createAutoBlockFuc {
    auto int a = 10;
    int(^block)(int) = ^int(int num) {
        return a * num;
    };
    a = 2;
    NSLog(@"auto block 的结果是 = %d", block(10)); // 100
}


@end
