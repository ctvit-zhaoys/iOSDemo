//
//  AppDelegate.h
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

