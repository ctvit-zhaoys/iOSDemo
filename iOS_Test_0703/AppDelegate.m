//
//  AppDelegate.m
//  iOS_Test_0703
//
//  Created by Yusheng Zhao on 2023/7/3.
//

#import "AppDelegate.h"
#import "CustomTabBarViewController.h"
#import "CommonTabBarViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
    // 自定义 TabBar (中间凸UI)
//    CustomTabBarViewController *vc = [[CustomTabBarViewController alloc]init];
    
    // 普通 TabBar
    CommonTabBarViewController *vc = [[CommonTabBarViewController alloc]init];

    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
